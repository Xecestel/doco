# Don't Open. COVID Outside

Made for the [Quarantine Jam 2020](https://itch.io/jam/quarantinejam).  
Find this game on [Itch.io](https://xecestel.itch.io/doco)!  

This is a 2D pixel art top-down bullet hell arcade videogame in which you play as a medic who has found the cure for the COVID-19 and has to give it to the infected people without getting infected himself.  

## #Don'tStayAtHome!

While everyone is locked on quarantine, a rogue medical researcher has found the cure to the fiendish virus. But there's no healthy human left to give the cure. No military, no politician, no one. Only him with the cure.  
He has no other choice than to go outside and face the virus by himself. But he's not immune and he's no hero, so he has to be careful. The virus can spread in two ways: by proximity and by coughing. He can't go near the infected, so how will he spread the cure?  

## Take your weapon, face your fear  
As the only scientist left, of course he has to be a genius. He managed to take a rifle and adapt it so that it can shoot syringes filled with the cure for the disease. This way, he just have to literally shoot the medicine to the infected to heal them, without any need of going near them. But he still has to be carefull: their breath is infected too.  
Avoid the infected coughs, don't come near them and kill the virus for good, once and for all, a syringe at a time.  

## Instructions

Move the character using the WASD Keys on your Keyboard or the LEft Stick on your Controller, aim with the mouse or the Right Stick and shoot with the left mouse button or the Right Trigger. Use the right button on your mouse or the Left Trigger to drop a granade. You can also pause the game with the Escape button on your Keyboard or the Start button on your Controller.  
Pay attention at your syringes: you can't carry too much of them.  
Don't worry, though, for some reason the infected people appear to leave behind useful stuff once you heal them.  

###  Mask filter

Of course, as a medic you have access to a professional filtered mask. It can actually prevent you from getting infected. But there's a catch: the filters don't last forever. Every hit you get will damage the used filter until it will become useless. The filter can take up to two hits, the third and last one will infect you. But don't worry! Of course you can _still_ find new filters around! I'm pretty sure that some of those infected actually got them before getting ill, so maybe if you heal them they will leave them behind for you. Keep an eye on your mask filter condition and be sure to take a new one if it's too damaged.  

### Syringes pack

This is exactly what it seems: a pack of syringes. They're basically your ammunition, so be sure to get them whenever you see them in the field to reload your rifle. Be careful not to finish them: without your shots you're helpless against the virus.  

### Cure grenades

I understand why you decided that a rifle was better than... _this_. I mean, maybe it's still a bit too much, but I think that you're some kind of The Walking Dead fanboy, are you? In any case, the rifle wasn't exactly your first choice. The _cure grenade_ was. Grenades filled with the cure for the virus. And you wanted to bomb the entire world with them. Do you also like Michael Bay, besides The Walking Dead? In any case, you actually crafted them in the end, and though they're not exactly... _conventional_, they can reaaally come in handy when you're surrounded. If you see one of these grenades somehow laying around the battlefield, take them. You can then throw them with the right button on your Mouse or the Left Trigger on your Controller. They should deal one... _heal damage_ to "every infected on the screen". That's what the instructions say anyway. Whatever it means.  
You can carry up to 2 grenades at a time.

## Don't take this mission too seriously

This game is made by an italian developer as a parody to try and lower the tension made by the COVID-19 pandemic and the following quarantine. It's not meant to be taken seriously.  
The events depicted in this game are entirely a work of fiction and aren't in any way related to real facts.  

## Now some really serious stuff
This game will be published on Itch for the Quarantine Jam. It is going to be completely free to play, no donation required. However, since humanity is facing one of its biggest challenge of the third millennium, I ask you to donate for a charity that is actually fighting the COVID-19. So that we can really win.  
Maybe before there's only one healthy man left, if we're lucky.  
So instead of making a donation to support me, make a donation to support them.

# Licenses

__Don't Open. COVID Outside__  

Copyright (C) 2020  Celeste Privitera  

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation, either version 3 of the License, or  
(at your option) any later version.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License  
along with this program.  If not, see <http://www.gnu.org/licenses/>.  

The "LICENSE" file refers to all parts of the code made for this game by Xecestel (Celeste Privitera), unless otherwhise stated somewhere inside the script or the addons directories.  

The graphic assets made by Celeste Privitera (@xecestel) are licensed under the Creative Commons Attribution-ShareAlike 4.0 Unported License.  
To view a copy of this license, visit [this page](http://creativecommons.org/licenses/by-sa/4.0/), or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA, or read the file "LICENSE_CC-BY-SA.txt" in this directory.  

Said assets are:
- `icon.png`
- The `Assets/Sprites` directory content (a part from the `./Doctor/Rifle.png` file

This game contains also third-party assets:
- The picture `Assets/Background/door.png` is a derivative work made by Federica Ress (@federica_ress) from a picture posted on [pxhere](https://pxhere.com/en/photo/1020015) and licensed under the [CC0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/) terms.
- The fonts `Assets/Allegreya Sans SC/AllegreyaSansSC-Bold.ttf`, `Assets/Allegreya Sans SC/AllegreyaSansSC-BoldItalic.ttf`, `Assets/Allegreya Sans SC/AllegreyaSansSC-MediumItalic.ttf` and `Assets/Allegreya Sans SC/AllegreyaSansSC-Medium.ttf` are part of the `Allegreya Sans SC Font Family` made by Juan Pablo de Peral for Huerta Tipografica and licensed under the [SIL Open Font License (OFL)](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)].
- The tileset `Assets/Tileset/roguelikeCity_magenta.png` is part of the __Roguelike Modern City Pack__ made by Kenney Vleugels for [Kenney](www.kenney.nl) and is licensed under the [CC0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/) terms.
- The sprite `Assets/Sprites/Doctor/Rifle.png` is a derivative work made by Celeste Privitera (@xecestel) from __Pixel Guns__, made by LakeKeta and licensed under the [CC-BY 3.0 Creative Commons Attribution 3.0](https://creativecommons.org/licenses/by/3.0/) license.
- The sprites `Assets/Sprites/Icons/mute.png` and `Assets/Sprites/Icons/volume.png` were made by Google and distributed under the [CC-BY 3.0 Creative Commons Attribution 3.0](https://creativecommons.org/licenses/by/3.0/) license terms.


# Credits
Desing and programming by Celeste Privitera (@xecestel).
Proudly made with Godot Engine 3 for the [Quarantine Jam 2020](https://itch.io/jam/quarantinejam).  

This game uses the [Sound Manager Plugin](https://gitlab.com/Xecestel/sound-manager) made by Celeste Privitera (@xecestel) and Simón Olivo (@sarturo).  
This game contains thirs party assets, such as:

#### Fonts

- __Exquisite Corpse__ by [Chad Savage](http://www.sinisterfonts.com/) (Public Domain)
- __Alegreya Sans SC Font Family__ by Juan Pablo de Peral for Huerta Tipografica ([SIL Open Font License (OFL)](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)])

#### Graphics

- __Photo of the doorway__ by [pxhere.com](https://pxhere.com/en/photo/1020015), modified by Federica Ress (@federica_ress) ([CC0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/))
- __Roguelike Modern City Pack__ by Kenney ([CC0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/))
- __Pixel Guns__ by LakeKeta, modified by Celeste Privitera (@xecestel) ([CC-BY 3.0 Creative Commons Attribution 3.0](https://creativecommons.org/licenses/by/3.0/))
- __Mute free icon__ and __Volume free icon__ by Google ([CC-BY 3.0 Creative Commons Attribution 3.0](https://creativecommons.org/licenses/by/3.0/))


#### Music

- __Energetic Metal Song__ by The Real Monoton Artist (CC0 Creative Commons License)
- __Menace__ by [yd](https://ydstuff.wordpress.com/) (CC0 Creative Commons License)

For more information, check the README files inside the respective directories.

# Changelog

### Version 0.0.1
- Initial commit

### Version 0.0.2
- Added Sound Manager Plugin
- Removed TransitionMgr
- Added audio buses

### Version 0.0.3
- Added music and sound effects
- Added fonts
- Updated Credits and README

### Version 0.0.4
- Updated README
- Added Sprites
- Added Player
- Added graphics license

### Version 0.0.5
- Added HUD

### Version 0.0.6
- Added Main Menu Background
- Completed HUD
- Added Game Over screen

### Version 0.0.7
- Added Explosion
- Added TileMap
- Added animations to player

### Version 0.0.8
- Added Enemies
- Added Spawns
- Added Grenades
- Added Powerups
- Fixed bugs

### Version 0.1
- Added Infected sprite and animation
- Added GitLab button
- Added Mask sprite
- Added sounds

### Version 1.0
- Game completed

### Version 1.1
- Difficulty balanced: grenades explode faster, enemies doesn't shoot at spawn anymore, you get more useful loot, grenades now kill all enemies on screen

### Version 1.2
- Difficulty balancement

### Version 1.3
- Added Controller support
- Added How to Play screen

### Version 1.3.1
- Bug fixes

### Version 1.3.2
- Added mute button and icons

### Version 1.4
- Added Pause menu
- Added Seppuku

### Version 1.5
- Randomized enemies' aim
- Now the player can carry up to two grenades instead of three
