extends CanvasLayer

#constants#
const MAIN_MENU_PATH = "res://Scenes/Menus/MainMenu/MainMenu.tscn";

#variables#
var score = 0;
var frames = 60;
var dead = false;

onready var hp_box = self.get_node("HP-Granades/HP");
onready var granades_box = self.get_node("HP-Granades/Granades");
onready var ammo_value = self.get_node("Ammo/Label");
onready var score_label = self.get_node("Score/Label");

onready var game_over_box = self.get_node("GameOverBox");
onready var final_score_label = self.get_node("GameOverBox/FinalScore");
onready var highscore_label = self.get_node("GameOverBox/Highscore");

onready var pause_menu = self.get_node("Pause");
###########

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SoundManager.play_bgm("BGM", 28.7);
	game_over_box.set_visible(false);
	highscore_label.set_visible(false);

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta : float) -> void:
	if (dead == false):
		if (frames == 0):
			score += 1;
			score_label.set_text(str(score));
			frames = 30;
		else:
			frames -= 1;
	else:
		if (Input.is_action_just_pressed("shoot")):
			self.get_tree().reload_current_scene();
		elif (Input.is_action_just_pressed("granade")):
			self.get_tree().change_scene(MAIN_MENU_PATH);

# Updates HP on screen
func update_hp(hp : int) -> void:
	var active_heart = hp_box.get_child(hp);
	var modulate = Color(0, 0, 0, 1) if active_heart.get_self_modulate() == Color(1, 1, 1, 1) else Color(1, 1, 1, 1);
	active_heart.set_self_modulate(modulate);

# Updates granades on screen
func update_granades(granades : int) -> void:
	var active_granade = granades_box.get_child(granades);
	var modulate = Color(0, 0, 0, 1) if active_granade.get_self_modulate() == Color(1, 1, 1, 1) else Color(1, 1, 1, 1);
	active_granade.set_self_modulate(modulate);

# Updates ammo on screen
func update_ammo(ammos : int) -> void:
	ammo_value.set_text(str(ammos));

# Handles game over
func game_over() -> void:
	dead = true;
	final_score_label.set_text(final_score_label.get_text() + str(score));
	game_over_box.set_visible(true);
	var new_high = Globals.set_highscore(score);
	highscore_label.set_visible(new_high);

# Opens the pause menu
func pause_game() -> void:
	pause_menu.pause_game();


#############################
#	SETTERS AND GETTERS		#
#############################

func get_score() -> int:
	return score;

#############################
#		SIGNAL HANDLING		#
#############################

# Fires when HP on Player change
func _on_Player_update_hp(hp : int) -> void:
	self.update_hp(hp);

# Fires when granades on Player change
func _on_Player_update_granades(granades : int) -> void:
	self.update_granades(granades);

# Fires when ammo on Player change
func _on_Player_update_ammo(ammos : int) -> void:
	self.update_ammo(ammos);

# Fires when Player dies
func _on_Player_death():
	self.game_over();

# Fires when an Infected dies
func _on_Infected_give_points(pts : int) -> void:
	score += pts;
	score_label.set_text(str(score));

# Fires when player pauses the game
func _on_Player_pause():
	self.pause_game();
