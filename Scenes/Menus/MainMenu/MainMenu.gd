extends Control

#constants#
const LEVEL_SCENE_PATH = "res://Scenes/Level/Level.tscn";
const CREDITS_SCENE_PATH = "res://Scenes/Menus/Credits/Credits.tscn";
const HOWTOPLAY_SCENE_PATH = "res://Scenes/Menus/HowToPlay/HowToPlay.tscn";
###########

#variables#
onready var play_button = self.get_node("Commands/PlayBtn");
onready var credits_button = self.get_node("Commands/CreditsBtn");
onready var quit_button = self.get_node("Commands/QuitBtn");
onready var command_box = self.get_node("Commands");
onready var title = self.get_node("Title");
onready var explosion_sprite = self.get_node("Explosion");
onready var copyright_label = self.get_node("Copyright");
###########


# Called when the node enters the scene tree for the first time.
func _ready():
	SaveLoad.load_game();
	SoundManager.play_bgm("MainMenuBGM");
	var copyright_text = copyright_label.get_text() + ProjectSettings.get("application/config/game_version");
	self.get_node("Copyright").set_text(copyright_text);
	play_button.grab_focus();

# Plays the explosion sound and animation
func explode() -> void:
	explosion_sprite.set_visible(true);
	explosion_sprite.play("explosion");
	title.set_visible(false);
	command_box.set_visible(false);
	copyright_label.set_visible(false);
	SoundManager.play_se("Explosion");

# When the Play Button is pressed
func _on_PlayBtn_pressed() -> void:
	SoundManager.stop_bgm();
	self.explode();

# Fires when the players press the HowToPlay button
func _on_HowToPlayBtn_pressed() -> void:
	self.get_tree().change_scene(HOWTOPLAY_SCENE_PATH);

# When the Credits Button is pressed
func _on_CreditsBtn_pressed() -> void:
	self.get_tree().change_scene(CREDITS_SCENE_PATH);

# When the Quit Button is pressed
func _on_QuitBtn_pressed() -> void:
	self.get_tree().quit();

# Fires when the animation finishes
func _on_Explosion_animation_finished() -> void:
	self.get_tree().change_scene(LEVEL_SCENE_PATH);

# Fires when the player presses the GitLab button
func _on_GitLabButton_pressed() -> void:
	if (OS.shell_open("https://gitlab.com/Xecestel/doco") != OK):
		print("Error opening given URL");

# Fires when the player presses the fullscreen button
func _on_FullScreenButton_pressed() -> void:
	OS.window_fullscreen = !OS.window_fullscreen;

# Fires when the Mute button gets pressed
func _on_MuteButton_toggled(button_pressed : bool) -> void:
	AudioServer.set_bus_mute(0, button_pressed);
