extends RigidBody2D


#variables#
var affected_enemies = [ ];

onready var sprite = self.get_node("Sprite");
onready var timer = self.get_node("Timer");
onready var explosion_sprite = self.get_node("ExplosionSprite");
onready var explosion_sound = self.get_node("ExplosionSound");
############

# Makes the granade explode
func explode() -> void:
	self.deal_damage();
	sprite.set_visible(false);
	explosion_sprite.set_visible(true);
	explosion_sprite.play("explosion");
	explosion_sound.play();

# Deals damage to all enemy in the AoE
func deal_damage() -> void:
	var bodies = self.get_parent().get_children();
	for body in bodies:
		if (body is KinematicBody2D && body.get_name().match("*Infected*")):
			body.deal_damage(3);

#########################
#	SIGNALS HANDLING	#
#########################

# Fires when the timer runs out
func _on_Timer_timeout():
	self.explode();

# Fires when the explosion animation has finished
func _on_ExplosionSprite_animation_finished():
	self.queue_free();
	pass
