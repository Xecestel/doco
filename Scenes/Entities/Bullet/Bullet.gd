extends RigidBody2D

#constants#
const SPEED = 850;
const BULLET_SPRITES_PATH = "res://Assets/Sprites/Bullets/";

#variables#
export (float) var damage	= 0.5;
export (String) var target_group;
export (String) var bullet_type;
export (Vector2) var direction;

var movement = Vector2.ZERO;

onready var hitbox	= self.get_node("Hitbox");
onready var sprite	= self.get_node("Sprite");
############

func _ready() -> void:
	self.set_bullet_texture();
#	Globals.debug_print(str(direction.angle()));
#	sprite.set_rotation_degrees(self.get_applied_force().angle() * PI/2);
	sprite.set_rotation_degrees(direction.angle() * PI/2);

func set_bullet(type : String) -> void:
	bullet_type = type;
	if (type == "vaccine"):
		self.set_collision_mask_bit(10, true);
		self.set_collision_mask_bit(0, false);
		self.set_collision_layer_bit(10, true);
		self.set_collision_layer_bit(0, false);
	elif (type == "virus"):
		self.set_collision_mask_bit(0, true);
		self.set_collision_mask_bit(10, false);
		self.set_collision_layer_bit(0, true);
		self.set_collision_layer_bit(10, false);

func set_direction(dir : Vector2) -> void:
	direction = dir;

func set_bullet_texture() -> void:
	var sprite_texture = load(BULLET_SPRITES_PATH + bullet_type + ".png");
	sprite.set_texture(sprite_texture);
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta : float) -> void:
	self.get_collisions();
	
func get_collisions() -> void:
	var colliders = self.get_colliding_bodies();
	for collider in colliders:
		if (collider is KinematicBody2D):
			collider.deal_damage(1);
		self.despawn();

# Makes the entity disappear from the game
func despawn() -> void:
	hitbox.set_disabled(false);
	self.queue_free();

func _on_DespawnTimer_timeout() -> void:
	self.despawn();
