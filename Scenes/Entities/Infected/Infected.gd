extends KinematicBody2D

#constants#
const SPEED = 150;
const BULLET_SCENE = "res://Scenes/Entities/Bullet/Bullet.tscn";

#variables#
export var hp = 1;

var movement = Vector2.ZERO;

onready var player = self.get_parent().get_node("Player");
onready var aim_raycast = self.get_node("AimRayCast");
onready var shoot_cooldown = self.get_node("ShootCooldown");
onready var cough = self.get_node("CoughSound");
onready var animation_player = self.get_node("AnimationPlayer");
###########

#signals#
signal give_points(pts);


# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect_signals();
	animation_player.play("running");
	shoot_cooldown.start();
	
func connect_signals() -> void:
	if (self.connect("give_points", self.get_parent().get_node("HUD"), "_on_Infected_give_points") != OK &&
		Globals.DEBUG):
		print_debug("An error occured while connecting give_points signal to HUD");


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta : float):
	if (player.is_dead()):
		self.set_process(false);
	else:
		self.aim();
		self.shoot();
		var collision = self.move(delta);
		self.get_collisions(collision);

# Takes aim on player
func aim() -> void:
	aim_raycast.set_cast_to(self.get_global_position().direction_to(player.get_global_position()));

# Shoots virus
func shoot() -> void:
	if (shoot_cooldown.time_left == 0):
			var bullet_scene = load(BULLET_SCENE);
			var bullet_node = bullet_scene.instance();
			bullet_node.position = self.get_global_position() + aim_raycast.get_cast_to().normalized()*50;
			#The aim vector gets randomized to make the infected less precise
			var aim_vector = self.randomize_vector(aim_raycast.get_cast_to().normalized(), 16);
			bullet_node.apply_impulse(Vector2(), aim_vector*500);
			bullet_node.set_direction(aim_vector);
			bullet_node.set_bullet("virus");
			self.get_parent().add_child(bullet_node);
			shoot_cooldown.start();
			cough.play();
	
# Moves the character
func move(delta : float) -> KinematicCollision2D:
	movement = aim_raycast.get_cast_to().normalized() * SPEED * delta;
	return self.move_and_collide(movement);

# Handles collisions
func get_collisions(collision : KinematicCollision2D) -> void:
	if (collision != null):
		var collider = collision.get_collider();
		if (collider == player):
			player.deal_damage(1);

# Handles damage
func deal_damage(dmg : int) -> void:
	hp = max(0, hp-dmg);
	if (hp == 0):
		self.die();

# Handles infected death
func die() -> void:
	randomize();
	var loot_id = randi()%8;
	self.spawn_loot(loot_id);
	self.emit_signal("give_points", 10);
	self.queue_free();

# Handles loot spawn
func spawn_loot(id : int) -> void:
	var loot_node = Globals.get_loot(id);
	if (loot_node != null):
		loot_node.position = self.get_global_position();
		self.get_parent().add_child(loot_node);

# Randomizes a vector using a given correction
func randomize_vector(vector : Vector2, correction_degrees : int) -> Vector2:
	randomize();
	var randomizer = randi()%correction_degrees;
	var aim_vector = vector;
	randomizer -= (correction_degrees/2 - 1);
	randomizer *= PI/180;
	aim_vector = aim_vector.rotated(randomizer);
	return aim_vector;
