extends Area2D

#constants#
const INFECTED_SCENE = "res://Scenes/Entities/Infected/Infected.tscn";
const MIN_SPAWN_TIME = 1;
############

#variables#
export (float) var spawn_time = 1;

var infected_scene = preload(INFECTED_SCENE);
var spawn_corrector = 1;

onready var spawn_timer = self.get_node("Timer");
###########


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	spawn_timer.start(spawn_time);

# Spawns a new infected
func make_infected() -> Node:
	var infected_node = infected_scene.instance();
	infected_node.position = self.get_global_position();
	return infected_node;

# Fires when the spawn timer runs out
func _on_Timer_timeout():
	spawn_corrector += 0.15;
	self.get_parent().add_child(self.make_infected());
	spawn_timer.start(max(spawn_time/spawn_corrector, MIN_SPAWN_TIME));

# Fires on player's death to stop the spawn
func _on_Player_death():
	spawn_timer.stop();
