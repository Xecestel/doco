extends KinematicBody2D

#constants#
const SPEED = 300;
const MAX_AMMO = 30;
const MAX_LIFEPOINTS = 3;
const MAX_GRANADES = 2;

const GRANADE_SCENE = "res://Scenes/Entities/Granade/Granade.tscn";
const BULLET_SCENE = "res://Scenes/Entities/Bullet/Bullet.tscn";

#variables#
var life_points : int = MAX_LIFEPOINTS;
var ammos		: int = MAX_AMMO;
var granades	: int = MAX_GRANADES;

var movement = Vector2.ZERO;
var controller_connected = false;

var invincible = false;
var moving = false;
var dead = false;

onready var left_arm = self.get_node("Body Parts/LeftArm");
onready var right_arm = self.get_node("Body Parts/RightArm");
onready var left_leg = self.get_node("Body Parts/LeftLeg");
onready var right_leg = self.get_node("Body Parts/RightLeg");

onready var aim_raycast = self.get_node("Body Parts/LeftArm/AimRayCast");
onready var animation_player = self.get_node("AnimationPlayer");
onready var damage_animation = self.get_node("DamageAnimation");

onready var shoot_cooldown = self.get_node("ShootCooldown");
onready var invincibility = self.get_node("InvincibilityTimer");
###########

#signals#
signal update_hp(hp);
signal update_ammo(ammos);
signal update_granades(granades);
signal death();
signal pause();
#########

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.follow_mouse();
	var direction = self.get_input(delta);
	var collider = self.move(delta, direction);
	self.get_collisions(collider);
	self.update_animation();

# Handles player's input
func get_input(delta : float) -> Vector2:
	moving = false;
	var direction = Vector2.ZERO;
	if (Input.is_action_pressed("up")):
		direction.y = -1;
		moving = true;
	if (Input.is_action_pressed("right")):
		direction.x = 1;
		moving = true;
	if (Input.is_action_pressed("down")):
		direction.y = 1;
		moving = true;
	if (Input.is_action_pressed("left")):
		direction.x = -1;
		moving = true;
	if (Input.is_action_pressed("shoot")):
		self.shoot();
	if (Input.is_action_just_pressed("granade")):
		self.throw_granade();
	if (Input.is_action_just_pressed("seppuku")):
		self.deal_damage(10);
	return direction;
#end

# Moves the character
func move(delta : float, direction : Vector2) -> KinematicCollision2D:
	movement = direction.normalized() * SPEED * delta;
	return self.move_and_collide(movement);
#end

# Gets collisions
func get_collisions(collider : KinematicCollision2D) -> void:
	if (collider != null):
		pass

# Makes the armed arm rotate following the mouse position
func follow_mouse() -> void:
	var vector = Vector2.ZERO;
	if (Globals.is_controller_connected() == false):
		vector = left_arm.get_global_position().direction_to(get_global_mouse_position());
	else:
		vector = Vector2(Input.get_joy_axis(0, JOY_AXIS_2), Input.get_joy_axis(0, JOY_AXIS_3));
	aim_raycast.set_cast_to(vector);
	left_arm.rotation_degrees = aim_raycast.get_cast_to().angle() * 180/PI - 90;

# Handles damage
func deal_damage(dmg : int) -> void:
	if (invincible):
		return;
	life_points = max(0, life_points-dmg);
	damage_animation.play("damage");
	SoundManager.play_se("Hit");
	self.emit_signal("update_hp", life_points);
	invincible = true;
	invincibility.start();
	if (life_points == 0):
		self.die();

# Handles player's death
func die() -> void:
	#death animation
	dead = true;
	self.emit_signal("death");
	self.set_process(false);

# Fires a shot
func shoot() -> void:
	if (self.ammos > 0):
		if (shoot_cooldown.time_left == 0):
			var bullet_scene = load(BULLET_SCENE);
			var bullet_node = bullet_scene.instance();
			
			bullet_node.position = left_arm.get_global_position() + aim_raycast.get_cast_to().normalized()*32;
			bullet_node.apply_impulse(Vector2(), aim_raycast.get_cast_to().normalized()*600);
			bullet_node.set_direction(aim_raycast.get_cast_to().normalized());
			bullet_node.set_bullet("vaccine");
			self.get_parent().add_child(bullet_node);
			self.ammos -= 1;
			emit_signal("update_ammo", ammos);
			SoundManager.play_se("Shoot");
			shoot_cooldown.start();
	else:
		SoundManager.play_se("NoAmmo");

# Throws a granade
func throw_granade() -> void:
	if (granades > 0):
		var granade_scene = load(GRANADE_SCENE);
		var granade_node = granade_scene.instance();
		
		granade_node.position = self.get_global_position();
		self.get_parent().add_child(granade_node);
		granades -= 1;
		self.emit_signal("update_granades", granades);

# Pauses game
func pause_game() -> void:
	emit_signal("pause");
	
func update_animation() -> void:
	if (moving):
		animation_player.set_speed_scale(1.5);
		animation_player.play("running");
	else:
		animation_player.set_speed_scale(1);
		animation_player.play("idle");

# Handles powerups
func powerup(id : int) -> void:
	if (id == Globals.POWERUPS.MASK):
		if (life_points < MAX_LIFEPOINTS):
			life_points += 1;
			emit_signal("update_hp", life_points-1);
	elif (id == Globals.POWERUPS.AMMO):
		ammos = MAX_AMMO;
		emit_signal("update_ammo", ammos);
	elif (id == Globals.POWERUPS.GRANADE):
		if (granades < MAX_GRANADES):
			granades += 1;
			emit_signal("update_granades", granades-1);

		
#########################
#	SETTERS & GETTERS	#
#########################

# Returns true if player is dead
func is_dead() -> bool:
	return dead;


#########################
# 	SIGNALS HANDLING	#
#########################

# Firse when the InvincibilityTimer runs out
func _on_InvincibilityTimer_timeout():
	invincible = false;
	
# Fires when the DamageAnimation player finish to play
func _on_DamageAnimation_animation_finished(anim_name : String):
	damage_animation.stop();

# Fires when Player touches a powerup
func _on_Powerup_power_up(powerup : int) -> void:
	self.powerup(powerup);
