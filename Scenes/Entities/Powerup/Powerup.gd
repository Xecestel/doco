extends Area2D

#constants#
const POWERUP_SPRITE_SCENES = ["", "res://Assets/Sprites/Power-Ups/Mask.png", "res://Assets/Sprites/Power-Ups/CureGranade.png","res://Assets/Sprites/Power-Ups/Ammo.png"]

#variables#
export var powerup = Globals.POWERUPS.NONE; # this variable uses the POWERUPS enum in the Globals script

onready var player = self.get_parent().get_node("Player");
onready var sprite = self.get_node("Sprite");
onready var area_shape = self.get_node("CollisionShape2D");
onready var despawn_timer = self.get_node("DespawnTimer");
onready var animation = self.get_node("AnimationPlayer");
###########

#signals#
signal power_up(id);
#########

# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect_signals();
	self.make_powerup();
	despawn_timer.start();
#end

func make_powerup() -> void:
	var sprite_texture = load(POWERUP_SPRITE_SCENES[powerup]);
	sprite.set_texture(sprite_texture);

func connect_signals() -> void:
	if (self.connect("power_up", player, "_on_Powerup_power_up") != OK):
		Globals.debug_print("Error connecting " + self.get_name() + " to Player");
#end
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (despawn_timer.get_time_left() <= 5):
		animation.play("almost_despawned");
	if (despawn_timer.get_time_left() <= 2):
		animation.play("almost_despawned", -1, 1.5);
#end

# Sets the type of powerup
func set_powerup(pu : int) -> void:
	self.powerup = pu;
#end

# Gets the type of powerup
func get_powerup() -> int:
	return self.powerup;
#end

#############################
#		SIGNAL HANDLING		#
#############################
# If the timer on DespawnTimer times out
func _on_DespawnTimer_timeout() -> void:
	self.queue_free();
#end

# If something touches the powerup
func _on_Powerup_body_entered(body : Node) -> void:
	if (body == player):
		self.emit_signal("power_up", powerup);
		self.queue_free();
#end
